﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace dp_hw6.Models
{
    public class dp_hw6Context : DbContext
    {
        public dp_hw6Context (DbContextOptions<dp_hw6Context> options)
            : base(options)
        {
        }

        public DbSet<dp_hw6.Models.Car> Car { get; set; }
    }
}
